#!/usr/bin/env bash
set -e

[ -z "$AWS_ACCESS_KEY_ID" ] && { echo "Need to set AWS_ACCESS_KEY_ID for AWS Access"; exit 1; }
[ -z "$AWS_SECRET_ACCESS_KEY" ] && { echo "Need to set AWS_SECRET_ACCESS_KEY for AWS Access"; exit 1; }
[ -z "$AWS_REGION" ] && { echo "Need to set AWS_REGION for AWS Region"; exit 1; }
[ -z "$EKS_CLUSTER_NAME" ] && { echo "Need to set EKS_CLUSTER_NAME for EKS Cluster name"; exit 1;}

if [ ! "$(eksctl get cluster --name ${EKS_CLUSTER_NAME})" ]; then
  echo "Cluster doesn't exist... You might want to setup cluster first..."
else
  eksctl utils write-kubeconfig --cluster ${EKS_CLUSTER_NAME}
fi

exec "$@"