FROM alpine:latest

RUN apk update \
    && apk add --update-cache --no-cache ca-certificates curl python3 bash \
    && pip3 install --upgrade pip \
    && pip3 install --upgrade --progress-bar off awscli

ENV KUBECTL_VERSION v1.17.2
RUN curl --silent --location "https://storage.googleapis.com/kubernetes-release/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl" -o /bin/kubectl \
    && chmod 755 /bin/kubectl

ENV HELM_VERSION=3.0.3
RUN curl --silent --location "https://get.helm.sh/helm-v${HELM_VERSION}-linux-amd64.tar.gz" | tar xvz -C /tmp \
    && mv -v /tmp/linux-amd64/helm /bin/helm \
    && chmod 755 /bin/helm

ENV EKSCTL_VERSION=0.13.0
RUN curl --silent --location "https://github.com/weaveworks/eksctl/releases/download/${EKSCTL_VERSION}/eksctl_Linux_amd64.tar.gz" | tar xz -C /tmp \
    && mv -v /tmp/eksctl /bin/eksctl \
    && chmod 755 /bin/eksctl

ADD . /eks-ci-stack
WORKDIR /eks-ci-stack

ENTRYPOINT [ "bin/entrypoint.sh" ]

CMD [ "/bin/bash" ]